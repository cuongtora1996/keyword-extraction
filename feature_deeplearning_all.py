from pyvi import ViPosTagger,ViTokenizer

from underthesea import pos_tag,ner,word_tokenize
import os
from gensim.models import Word2Vec
os.environ['KERAS_BACKEND'] = 'tensorflow'
from collections import Counter
from pathlib import Path
import re,math,pprint,pickle,sys,json,random
import tensorflow as tf
sess = tf.Session()
import numpy as np
from nltk.chunk import RegexpParser
from keras.models import Sequential,load_model
from keras.layers import Dense, LSTM, Bidirectional, GRU, Conv1D, MaxPooling1D, Flatten, Dropout,SimpleRNN
from keras import metrics
from keras import backend as K
from keras.callbacks import CSVLogger,ModelCheckpoint
from imblearn.over_sampling import SMOTE
import random
from string import punctuation
from extract_candidate_keyword import extract_candidate_keywords
from extract_feature import get_features
from keras_metric_custom import fmeasure,recall,precision,tp,fp,tn,fn
from compare_keyword import compare
import fnmatch

#Get all text
length = 0
count = 0
all_text = []
all_text_train =[]
all_text_validate = []
keyword_list = []
feature_list = []
result_list = []
val_feature_list = []
val_result_list = []
n = 0
with open("stopwords.txt", "r", encoding="utf8") as f:
    all_stop_words = f.read()
    all_stop_words = all_stop_words.split("\n")
for dir_path,dir_names,file_names in os.walk("data2/Train"):
    for dir in dir_names:
        length += len(fnmatch.filter(os.listdir(os.path.join(dir_path, dir)), "*.txt"))
    lenfile = len(file_names)
    start = count
    for file in file_names:
        count += 1
        with open(os.path.join(dir_path, file), 'r+', encoding="utf8") as f:
            text = f.read()
            if (round((count - start) * 100 / lenfile, 2)) >= 88.9:
                all_text_validate.append(json.loads(text))
            else:
                all_text_train.append(json.loads(text))
            all_text.append(json.loads(text))

            sys.stdout.write("\r Process: " + str(round(count * 100 / length, 2)) + "%")
            sys.stdout.flush()


#End ===

#Get candidates and features
word2vec_model = Word2Vec.load("word2vec_model_final2")

max = 0
count = 0
all_candidates_appear_times = []

for text in all_text_train:
    candidates = extract_candidate_keywords(text['content'])
    count += 1
    print(count)
    text_lower = " ".join([w.lower() for w in text['content']])

    filter_candidates = []
    for candidate in set(candidates):
        if candidate.lower() not in [w.lower() for w in filter_candidates]:
            filter_candidates.append(candidate)

    print(filter_candidates)
    all_candidates_appear_times.extend([candidate.lower() for candidate in candidates])
    max_fthema = 0
    max_fwlpl = 0
    current_feature_list = []
    for candidate in filter_candidates:

        n_document = len([candidate for t in all_text if candidate.lower() in t['content'].lower()])

        idf = math.log(len(all_text)/n_document,10)

        candidate_vector = np.zeros(shape=(1, 300))
        for w in candidate.split():
            s = w
            if w[-1] in punctuation:
                s = w[0:-1]
            if s!='':
                candidate_vector+= np.array(word2vec_model.wv[s])

        features = get_features(idf, text['content'], candidate, candidates, candidate_vector)
        if len(features) > max:
            max=len(features)
        if features[300] > max_fthema:
            max_fthema = features[300]
        if features[301] > max_fwlpl:
            max_fwlpl = features[301]
        current_feature_list.append(features)

        if candidate.lower() in [w.lower() for w in text['keyword']]:
            result_list.append(1)

        else:
            flag = True
            for w in candidate.lower().split(" "):
                if w not in [a.lower() for a in text['keyword']]:
                    flag = False
            if flag == True:
                result_list.append(1)

            else:

                keys = text['keyword']
                f_max = compare(candidate,keys)

                if (f_max>=0.5):
                    result_list.append(1)

                else:
                    result_list.append(0)


    for t in current_feature_list:
        t[300] = t[300]/max_fthema
        t[301] = t[301]/max_fwlpl

        feature_list.append(t)




    sys.stdout.write("\r Process: " + str(round(count * 100 / (len(all_text)+len(all_text_validate)), 2)) + "%")
    sys.stdout.flush()

print("\n~~~~~~~~~~~~~~~~~~~~~~~Finish Train~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")

for text in all_text_validate:
    candidates = extract_candidate_keywords(text['content'])
    count += 1
    print(count)
    text_lower = " ".join([w.lower() for w in text['content']])

    filter_candidates = []
    for candidate in set(candidates):
        if candidate.lower() not in [w.lower() for w in filter_candidates]:
            filter_candidates.append(candidate)

    print(filter_candidates)
    all_candidates_appear_times.extend([candidate.lower() for candidate in candidates])
    max_fthema = 0
    max_fwlpl = 0
    current_feature_list = []
    for candidate in filter_candidates:

        n_document = len([candidate for t in all_text if candidate.lower() in t['content'].lower()])

        idf = math.log(len(all_text)/n_document,10)

        candidate_vector = np.zeros(shape=(1, 300))
        for w in candidate.split():
            s = w
            if w[-1] in punctuation:
                s = w[0:-1]
            if s!='':
                candidate_vector+= np.array(word2vec_model.wv[s])

        features = get_features(idf, text['content'], candidate, candidates, candidate_vector)
        if len(features) > max:
            max=len(features)
        if features[300] > max_fthema:
            max_fthema = features[300]
        if features[301] > max_fwlpl:
            max_fwlpl = features[301]
        current_feature_list.append(features)

        if candidate.lower() in [w.lower() for w in text['keyword']]:
            val_result_list.append(1)

        else:
            flag = True
            for w in candidate.lower().split(" "):
                if w not in [a.lower() for a in text['keyword']]:
                    flag = False
            if flag == True:
                val_result_list.append(1)

            else:

                keys = text['keyword']
                f_max = compare(candidate,keys)

                if (f_max>=0.5):
                    val_result_list.append(1)

                else:
                    val_result_list.append(0)


    for t in current_feature_list:
        t[300] = t[300]/max_fthema
        t[301] = t[301]/max_fwlpl

        val_feature_list.append(t)




    sys.stdout.write("\r Process: " + str(round(count * 100 / (len(all_text)+len(all_text_validate)), 2)) + "%")
    sys.stdout.flush()

print("\n~~~~~~~~~~~~~~~~~~~~~~~Finish Validate~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
from collections import Counter


ct = Counter(all_candidates_appear_times)
np.save('idf_r3_normalize.npy', ct)


#end ====

#turn to vector

seq_len = max

x_train = np.zeros((len(feature_list), seq_len), dtype=float)
for i, row in enumerate(feature_list):
    x_train[i,-len(row):] = np.array(row)[:seq_len]

y_train = np.array(result_list)
print(len(feature_list))

print(Counter(y_train))

x_val = np.zeros((len(val_feature_list), seq_len), dtype=float)
for i, row in enumerate(val_feature_list):
    x_val[i, -len(row):] = np.array(row)[:seq_len]


y_val = np.array(val_result_list)
print(len(val_feature_list))

print(Counter(y_val))

sm = SMOTE(kind='regular')


x_train,y_train = sm.fit_sample(x_train,y_train)
su = list(zip(x_train,y_train))
random.shuffle(su)
x_train,y_train = zip(*su)
x_train = np.asarray(x_train)
y_train = np.asarray(y_train)

np.save("x_train_r3_normalize.npy",x_train)
np.save("y_train_r3_normalize.npy",y_train)
np.save("x_val_r3_normalize.npy",x_val)
np.save("y_val_r3_normalize.npy",y_val)
x_train = np.load("x_train_r3_normalize.npy")
y_train = np.load("y_train_r3_normalize.npy")
x_val = np.load("x_val_r3_normalize.npy")
y_val = np.load("y_val_r3_normalize.npy")
x_train_rs = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))
x_val_rs = np.reshape(x_val, (x_val.shape[0], x_val.shape[1], 1))

print("\n\t\t\tFeature Shapes:")
print("Train set: \t\t{}".format(x_train.shape),
      "\nValidate set:\t\t{}".format(x_val.shape),
      "\nTest train set: \t\t{}".format(y_train.shape),
      "\nTest validate set:\t\t{}".format(y_val.shape)
      )
print(Counter(y_train))
print(Counter(y_val))

print(Counter(list(y_train)))
print(Counter(list(y_val)))


csv_logger = CSVLogger('log/log_CNN_r3_normalize.csv', append=True, separator=';')
check_point = ModelCheckpoint("model/CNN_model_r3_normalize.h5", verbose=1, save_best_only=False, save_weights_only=False,
                              mode='auto')

model = Sequential()
model.add(Conv1D(filters=128,kernel_size=5, input_shape=(x_train.shape[1], 1),activation="relu"))
model.add(MaxPooling1D(pool_size=4))
model.add(Flatten())
model.add(Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy',precision, recall, fmeasure])

print(model.summary())
# save model to single file
model.fit(x_train_rs, y_train, epochs=1000, batch_size=500, callbacks=[csv_logger, check_point], verbose=1,
          validation_data=(x_val_rs, y_val))
K.clear_session()

csv_logger = CSVLogger('log/log_LSTM_r3_normalize.csv', append=True, separator=';')
check_point = ModelCheckpoint("model/LSTM_model_r3_normalize.h5", verbose=1, save_best_only=False, save_weights_only=False,
                              mode='auto')

model = Sequential()
model.add(LSTM(128, recurrent_dropout=0.4, dropout=0.4, input_shape=(x_train.shape[1], 1)))
model.add(Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy',precision, recall, fmeasure])

print(model.summary())
# save model to single file
model.fit(x_train_rs, y_train, epochs=1000, batch_size=200, callbacks=[csv_logger, check_point], verbose=1,
          validation_data=(x_val_rs, y_val))
K.clear_session()



csv_logger = CSVLogger('log/log_Bi_LSTM_r3_normalize.csv', append=True, separator=';')
check_point = ModelCheckpoint("model/Bi_LSTM_model_r3_normalize.h5", verbose=1, save_best_only=False, save_weights_only=False, mode='auto')
model = Sequential()
model.add(Bidirectional(LSTM(128,recurrent_dropout=0.4,dropout=0.4),input_shape=(x_train.shape[1],1)))

model.add(Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy',precision,recall,fmeasure])

print(model.summary())
#save model to single file

model.fit(x_train_rs, y_train, epochs=782, batch_size=500,callbacks=[csv_logger,check_point],verbose=1,validation_data=(x_val_rs,y_val))


#
#
# # load model from single file
# # model = load_model('model/LSTM_model.h5',custom_objects={"precision":precision,"recall":recall,"fmeasure":fmeasure})
# # print(model.summary())
#
#
# #Final evaluation of the model
# # scores = model.evaluate(val_x_rs, val_y)
# # print("Accuracy Validation: %.2f%%" % (scores[1]*100))
# #
