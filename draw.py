import csv
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from pylab import *
def drawLineGraphForOne(fileName, type):
    listValue = []
    type = type.lower()
    with open(fileName, newline='') as csvfile:
        header = []
        spamreader = csv.reader(csvfile, delimiter=';')
        i = 0
        for row in spamreader:
            if not header:
                header = row
            elif i % 10 == 0 or i == 499:
                row = list(map(float, row))
                for x in range(len(header)):
                    check = header[x].lower()
                    if check == type:
                        listValue.append(row[x])

            i += 1

    t = [i*10 for i in range(51)]
    if type == 'acc':
        plt.plot(t, listValue, color='green')
    elif type == 'fmeasure':
        plt.plot(t, listValue, color='blue')
    elif type == 'loss':
        plt.plot(t, listValue, color='red')
    elif type == 'precision':
        plt.plot(t, listValue, color='orange')
    elif type == 'recall':
        plt.plot(t, listValue, color='yellow')

    plt.xlabel('Epoch')
    if type =='acc':
        type = 'accuracy'
    elif type == 'fmeasure':
        type = 'f1 score'
    plt.ylabel(type)
    type = type[0].upper()+type[1:]
    plt.title(type+' chart')
    plt.grid(True)
    nameSave = 'LineGraph_' + type+ '.png'
    savefig(nameSave)
    plt.figure()
    plt.show()

#drawLineGraphForOne('log_LSTM_Dropout.csv','precision')

def drawLineGraphForMulti(fileName, listType):
    val_type=dict()
    for type in listType:
        val_type[type]=[]
    with open(fileName, newline='') as csvfile:
        header = []
        spamreader = csv.reader(csvfile, delimiter=';')
        i = 0
        for row in spamreader:
            if not header:
                header = row
            elif i % 10 == 0 or i == 999:
                row = list(map(float, row))
                for x in range(len(header)):
                    check = header[x].lower()
                    if check in listType:
                        val_type[check].append(row[x])
            i += 1

    t = [i*10 for i in range(101)]
    colors = ['green','black','blue','red','pink','orange','brown']
    handles = []
    for i in range(len(listType)):
        plt.plot(t, val_type[listType[i]], color=colors[i])
        patch = mpatches.Patch(color=colors[i], label=listType[i])
        handles.append(patch)
    plt.legend(handles=handles)
    plt.xlabel('Epoch')
    plt.ylabel('Value')
    plt.title('Summary chart')
    plt.grid(True)
    nameSave='LineGraph'
    for type in listType:
        nameSave = nameSave+'_'+type
    nameSave = nameSave+'.png'
    savefig(nameSave)
    plt.figure()
    plt.show()
drawLineGraphForMulti('log/log_CNN_r3_normalize_nb.csv',['acc','recall','precision','fmeasure'])
drawLineGraphForMulti('log/log_CNN_r3_normalize_nb.csv',['val_acc','val_fmeasure','val_recall','val_precision'])
def drawLineGraphForMultiFile(listfileName,type):
    listValue = []
    type = type.lower()
    color=['blue','red','orange','green']
    for filename in listfileName:

        with open(filename, newline='') as csvfile:
            header = []
            spamreader = csv.reader(csvfile, delimiter=';')
            temp = 0
            value = []
            for row in spamreader:
                if not header:
                    header = row
                elif temp % 10 == 0 or temp == 999:
                    row = list(map(float, row))
                    for x in range(len(header)):
                        check = header[x].lower()
                        if check == type:
                            value.append(row[x])

                temp += 1
            listValue.append(value)
    t = [i * 10 for i in range(101)]

    handles = []
    for i in range(len(listfileName)):

        plt.plot(t, listValue[i], color=color[i])
        path = mpatches.Patch(color=color[i], label=listfileName[i].replace("log_","").replace(".csv",""))
        handles.append(path)
    plt.legend(handles=handles)
    plt.xlabel('Epoch')
    if type == 'acc':
        type = 'accuracy'
    elif type == 'fmeasure':
        type = 'f1 score'
    plt.ylabel(type)
    type = type[0].upper() + type[1:]
    plt.title(type + ' chart')
    plt.grid(True)
    plt.figure()
    plt.show()

#drawLineGraphForMultiFile(['log/log_Bi_LSTM_Dense.csv','log/log_Bi_LSTM.csv'],'acc')