from prettytable import PrettyTable
from pyvi import ViPosTagger,ViTokenizer

from underthesea import pos_tag,ner,word_tokenize
import os
from gensim.models import Word2Vec
os.environ['KERAS_BACKEND'] = 'tensorflow'
from collections import Counter
from pathlib import Path
import re,math,pprint,pickle,sys,json,random
import tensorflow as tf
sess = tf.Session()
import numpy as np
from nltk.chunk import RegexpParser
from keras.models import Sequential,load_model
from keras.layers import Dense, LSTM, Bidirectional, GRU, Conv1D, MaxPooling1D, Flatten, Dropout,SimpleRNN
from keras import metrics
from keras import backend as K
from keras.callbacks import CSVLogger,ModelCheckpoint
from imblearn.over_sampling import SMOTE
import random
from string import punctuation
from extract_candidate_keyword import extract_candidate_keywords
from extract_feature import get_features
from keras_metric_custom import fmeasure,recall,precision
from compare_keyword import compare
import fnmatch

#Get all text
length = 0
count = 0
all_text = []
keyword_list = []
feature_list = []
result_list = []
n = 0
with open("stopwords.txt", "r", encoding="utf8") as f:
    all_stop_words = f.read()
    all_stop_words = all_stop_words.split("\n")
for dir_path,dir_names,file_names in os.walk("data2/Test"):
    for dir in dir_names:
        length += len(fnmatch.filter(os.listdir(os.path.join(dir_path, dir)), "*.txt"))

    for file in file_names:
        count += 1
        with open(os.path.join(dir_path, file), 'r+', encoding="utf8") as f:
            text = f.read()
            all_text.append(json.loads(text))

            sys.stdout.write("\r Process: " + str(round(count * 100 / length, 2)) + "%")
            sys.stdout.flush()


#End ===

#Get candidates and features
word2vec_model = Word2Vec.load("word2vec_model_final2")


idfs = dict(np.load('idf_r3_normalize.npy').item())
max = 0
count = 0

for text in all_text:
    candidates = extract_candidate_keywords(text['content'])
    count += 1
    print(count)
    text_lower = " ".join([w.lower() for w in text['content']])

    filter_candidates = []
    for candidate in set(candidates):
        if candidate.lower() not in [w.lower() for w in filter_candidates]:
            filter_candidates.append(candidate)

    print(filter_candidates)

    max_fthema = 0
    max_fwlpl = 0
    current_feature_list = []
    for candidate in filter_candidates:

        if candidate in idfs:
            idf = math.log(300/idfs[candidate],10)
        else:
            idf = math.log(301,10)

        a = np.zeros(shape=(1, 300))
        for w in candidate.split():
            s = w
            if w[-1] in punctuation:
                s = w[0:-1]
            if s != '' and s in word2vec_model.wv.vocab.keys():
                a+= np.array(word2vec_model.wv[s])

        features = get_features(idf,text['content'],candidate,candidates,a)
        if len(features) > max:
            max=len(features)
        if features[300] > max_fthema:
            max_fthema = features[300]
        if features[301] > max_fwlpl:
            max_fwlpl = features[301]
        feature_list.append(features)

        if candidate.lower() in [w.lower() for w in text['keyword']]:
            result_list.append(1)

        else:
            flag = True
            for w in candidate.lower().split(" "):
                if w not in [a.lower() for a in text['keyword']]:
                    flag = False
            if flag == True:
                result_list.append(1)

            else:
                keys = text['keyword']
                f_max = compare(candidate, keys)

                if (f_max >= 0.5):
                    result_list.append(1)

                else:
                    result_list.append(0)


    for t in current_feature_list:
        t[300] = t[300]/max_fthema
        t[301] = t[301]/max_fwlpl
        feature_list.append(t)



    sys.stdout.write("\r Process: " + str(round(count * 100 / len(all_text), 2)) + "%")
    sys.stdout.flush()



#end ====

#turn to vector

seq_len = max

features = np.zeros((len(feature_list), seq_len), dtype=float)
for i, row in enumerate(feature_list):
    features[i, -len(row):] = np.array(row)[:seq_len]

labels = np.array(result_list)
print(len(feature_list))

print(Counter(labels))
print(seq_len)


model = load_model('model/Bi_LSTM_model_r3_normalize.h5', custom_objects={"precision": precision, "recall": recall, "fmeasure": fmeasure})


features = np.reshape(features,(features.shape[0],features.shape[1],1))
scores = model.evaluate(features,labels)
print('loss=%f, acc=%f, precision=%f, recall=%f, fmeasure=%f' % (scores[0],scores[1],scores[2],scores[3],scores[4]))

thresh_hold = [0.3,0.4,0.5,0.6,0.7]

for t in thresh_hold:
    scores = model.predict(features)
    result_list2 = []
    for i in scores:
        if i>=t : result_list2.append(1)
        else: result_list2.append(0)
    labels2 = K.variable(np.array(result_list2))

    print("Thresh_hold : %f , F1-score : %f" % (t,K.eval(fmeasure(K.variable(labels),labels2))))

    t = PrettyTable(['','0', '1','total'])
    TN=0
    FP=0
    TP=0
    FN=0
    for i in range(len(result_list2)):
        if result_list2[i] == 0:
            if result_list[i] == 0:
                TN +=1
            else:
                FN +=1
        else:
            if result_list[i] == 0:
                FP +=1
            else:
                TP +=1
    t.add_row(["0", TN, FP,TN+FP])
    t.add_row(["1", FN, TP, FN + TP])
    t.add_row(["total", TN+FN, TP+FP, ''])
    print(t)

