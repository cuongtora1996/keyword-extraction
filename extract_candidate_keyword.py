from pyvi import ViPosTagger,ViTokenizer
from underthesea import pos_tag,ner,word_tokenize
import os,fnmatch

os.environ['KERAS_BACKEND'] = 'tensorflow'
import json,random
from collections import Counter
import sys
import pickle
from pathlib import Path
import re
import math
import pprint
import numpy as np
import nltk
from nltk.chunk import RegexpParser
import random
from string import punctuation
from pos_tag_custom import postagging
with open("stopwords.txt", "r", encoding="utf8") as f:
    all_stop_words = f.read()
    all_stop_words = all_stop_words.split("\n")
    
def extract_candidate_keywords(document):
    a = ViPosTagger.postagging(document)
    tagged = []
    for i, word in enumerate(a[0]):
        tagged.append((word, a[1][i]))


    chunkGram = r""" PHRASE:
                        {(<N.*>+ <A>* <E>)? <N.*>+ <A>*}
                """
    chunkParser = RegexpParser(chunkGram)
    chunked = chunkParser.parse(tagged)
    candidate_keywords = []
    for tree in chunked.subtrees():
        if tree.label() == 'PHRASE':
            candidate_keyword = ' '.join([x for x,y in tree.leaves()])
            candidate_keywords.append(candidate_keyword.strip())
            # flag_start = True
            # name = ""
            # for n in ner(candidate_keyword):
            #     if flag_start == False:
            #         if n[3] != "O":
            #             flag_start = True
            #             name = n[0]
            #     else:
            #         if n[3] != "O":
            #             name += " " + n[0]
            #         else:
            #             flag_start = False
            #             name = re.sub(r"(\w)(\.)(\s)(?=(?:(\w)(?:[\.\s]|$)))", r'\1\2', name)
            #             candidate_keywords.append(
            #                 name.replace(" / ", "/").replace(" - ", "-").replace(" & ", "&").strip())
            #             name = ""
            #
            # if (flag_start == True):
            #     name = re.sub(r"(\w)(\.)(\s)(?=(?:(\w)(?:[\.\s]|$)))", r'\1\2', name)
            #     candidate_keywords.append(name.replace(" / ", "/").replace(" - ", "-").replace(" & ", "&").strip())
            #

    candidate_keywords = [key for key in candidate_keywords if len(key)>1 and  len(key.split(' ')) < 5 and key.lower() not in all_stop_words]

    return candidate_keywords


#Get all text
# length = 0
# count = 0
# all_text = []
# keyword_list = []
# feature_list = []
# result_list = []
# n = 0
# with open("stopwords.txt", "r", encoding="utf8") as f:
#     all_stop_words = f.read()
#     all_stop_words = all_stop_words.split("\n")
# for dir_path,dir_names,file_names in os.walk("data2/Train"):
#     for dir in dir_names:
#         length += len(fnmatch.filter(os.listdir(os.path.join(dir_path, dir)), "*.txt"))
#
#     for file in file_names:
#         count += 1
#         with open(os.path.join(dir_path, file), 'r+', encoding="utf8") as f:
#             text = f.read()
#             all_text.append(json.loads(text))
#
#             sys.stdout.write("\r Process: " + str(round(count * 100 / length, 2)) + "%")
#             sys.stdout.flush()
#
#
#
# all_candidates_appear_times = []
#
# for text in all_text:
#     candidates = extract_candidate_keywords(text['content'])
#     filter_candidates = []
#     for candidate in set(candidates):
#         if candidate.lower() not in [w.lower() for w in filter_candidates]:
#             filter_candidates.append(candidate)
#     all_candidates_appear_times.extend(filter_candidates)
# with open('report/candidate_keywords2.txt','w+',encoding="utf8") as f:
#     report = [0]*10
#     for w in all_candidates_appear_times:
#         f.write(w+"\n")
#         c = w.count(' ')
#         report[c + 1] += 1
#
#     for r in range(1,10):
#         if(report[r]!=0):
#             f.write(str(r)+'từ:'+str(report[r])+',')
#
#     f.close()
#
