import matplotlib.pyplot as plt
from sklearn.datasets import make_classification
from sklearn.decomposition import PCA
import numpy as np
from imblearn.over_sampling import SMOTE



def plot_resampling(ax, X, y, title):

    c0 = ax.scatter([X[i,0]for i in range(len(y)) if y[i]==0],[X[i,1]for i in range(len(y)) if y[i]==0], label="Class #0", alpha=0.5)
    c1 = ax.scatter([X[i,0]for i in range(len(y)) if y[i]==1],[X[i,1]for i in range(len(y)) if y[i]==1], label="Class #1", alpha=0.5)
    ax.set_title(title)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    ax.spines['left'].set_position(('outward', 10))
    ax.spines['bottom'].set_position(('outward', 10))
    ax.set_xlim([-10, 10])
    ax.set_ylim([-2.5, 2.5])

    return c0, c1

def fit_over_sampling(X,y):
    sm = SMOTE(kind='svm')
    X_resampled = []
    y_resampled = []

    X_res, y_res = sm.fit_sample(X, y)
    print(type(X_res))
    X_resampled.extend(X_res)
    y_resampled.extend(y_res)
    return X_resampled,y_resampled

#Generate the dataset
#
# X = np.load("features2.npy")
# y = np.load("labels2.npy")
# from collections import Counter
#
# ct = Counter(y)
# print(ct[0],"&",ct[1])
#
# # Instanciate a PCA object for the sake of easy visualisation
# pca = PCA(n_components=2)
# # Fit and transform x to visualise inside a 2D feature space
# X_vis = pca.fit_transform(X)
#
# # Apply regular SMOTE
# sm = SMOTE(kind='svm')
# X_resampled = []
# y_resampled = []
#
#
# X_res, y_res = sm.fit_sample(X, y)
# X_resampled.extend(X_res)
# y_resampled.extend(y_res)
# X_res_vis = pca.transform(X_res)
#
# # Two subplots, unpack the axes array immediately
# # Remove axis for second plot
# ax1 = plt.gca()
# c0, c1 = plot_resampling(ax1, X_vis, y, 'Original set')
# plt.show()
# ct = Counter(y_resampled)
# print(ct[0],"&",ct[1])
# ax3 = plt.gca()
# plot_resampling(ax3, X_res_vis, y_resampled,'SMOTE regular')
# #
# # ax2.legend((c0, c1), ('Class #0', 'Class #1'), loc='center',
# #            ncol=1, labelspacing=0.)
# plt.tight_layout()
# plt.show()