import os
import sys
import fnmatch
import json
from pyvi import ViTokenizer
from string import punctuation
from underthesea import pos_tag,word_tokenize

import io
import re
def tokenizeFile():
    length = 0
    count = 0

    for dir_path,dir_names,file_names in os.walk(".\Re-data"):
        for dir in dir_names:
            length += len(fnmatch.filter(os.listdir(os.path.join(dir_path,dir)),"*.txt"))


        for file in file_names:
            count +=1
            print(file)

            with io.open(os.path.join(dir_path, file), 'r+', encoding="utf-8-sig") as f:
                text = f.read()
                textj = json.loads(text)

            target_path = dir_path.replace("pre_data","data2")

            if not os.path.exists(target_path):
                os.makedirs(target_path)
            with open(os.path.join(target_path,file),'w+',encoding="utf8") as f:
                text_all = textj['content'].strip().replace(".\n"," . ").replace("\n"," . ").replace(" - "," 0-0 ").replace(" / "," 0/0 ").replace(" & "," 0&0 ")

                textcontent = ViTokenizer.tokenize(text_all)
                lower_textcontent = [w.lower().replace("_"," ") for w in textcontent.split()]
                keywords = []
                for keyword in textj['keyword']:
                    if keyword.lower() in lower_textcontent:
                        keyword_after = keyword.replace(" ","_")
                        keywords.append(keyword_after)
                    else:
                        keyword_after = ViTokenizer.tokenize(keyword)
                        if "_" in keyword_after:
                            if(keyword!=keyword_after):
                                find_unnderscore = [pos for pos, char in enumerate(keyword_after) if char == "_"]

                                while keyword.lower() in text_all.lower():

                                    pos = text_all.lower().find(keyword.lower())
                                    if(pos!=-1):
                                        text_need = text_all[pos:len(keyword)+pos]



                                        temp = text_need
                                        for i in find_unnderscore:

                                            temp = temp[:i] +"_"+temp[i+1:]

                                        text_all=text_all.replace(text_need,temp)
                            keywords.append(keyword_after)
                        else:
                            keywords.append(keyword)

                textcontent = word_tokenize(text_all,format="text")
                splash =" / "
                middle =" - "
                andsym =" & "

                textcontent = textcontent.replace(splash,"/").replace(middle,"-").replace(andsym,"&").replace("0-0","-").replace("0/0","/").replace("0&0","&").replace(" ’ ","’")
                textcontent = re.sub(r"([a-zA-Z])(\.)(\s)(?=(?:([a-zA-Z])(?:[\.\s])))",r'\1\2',textcontent)
                print(textcontent)
                data = {"link":textj['link'],"content":textcontent,"keyword":keywords}
                f.write(json.dumps(data,ensure_ascii=False))
                f.close()
                sys.stdout.write("\r Process: "+str(round(count*100/length,2))+"%...")
                sys.stdout.flush()

print("Parse resource to data: ")
tokenizeFile()
print("=Done=")
