from pyvi import ViPosTagger,ViTokenizer
from underthesea import pos_tag,ner,word_tokenize
import os,fnmatch

import json,random
from collections import Counter
import sys
import pickle
from pathlib import Path
import re
import math
import pprint
import numpy as np
import nltk
from nltk.chunk import RegexpParser
import random
from string import punctuation
from pos_tag_custom import postagging
from extract_candidate_keyword import extract_candidate_keywords

import string
#Get all text
length = 0
count = 0
all_text = []
keyword_list = []
feature_list = []
result_list = []
n = 0
with open("stopwords.txt", "r", encoding="utf8") as f:
    all_stop_words = f.read()
    all_stop_words = all_stop_words.split("\n")
for dir_path,dir_names,file_names in os.walk("data2/Train"):
    for dir in dir_names:
        length += len(fnmatch.filter(os.listdir(os.path.join(dir_path, dir)), "*.txt"))

    for file in file_names:
        count += 1
        with open(os.path.join(dir_path, file), 'r+', encoding="utf8") as f:
            text = f.read()
            all_text.append(json.loads(text))

            sys.stdout.write("\r Process: " + str(round(count * 100 / length, 2)) + "%")
            sys.stdout.flush()



all_candidates_appear_times = []
max_can = 0
min_can = float('inf')
max_word = 0
min_word = float('inf')
ave_word = 0
translator = str.maketrans('', '', string.punctuation)


for text in all_text:
    candidates = extract_candidate_keywords(text['content'])
    filter_candidates = []
    for candidate in set(candidates):
        if candidate.lower() not in [w.lower() for w in filter_candidates]:
            filter_candidates.append(candidate)
    if len(filter_candidates) > max_can:
        max_can = len(filter_candidates)
    if len(filter_candidates) <min_can:
        min_can = len(filter_candidates)
    temp = text['content'].translate(translator)
    if len(temp.split()) > max_word:
        max_word = len(temp.split())
    if len(temp.split()) < min_word:
        min_word = len(temp.split())
    ave_word += len(temp.split())

print("Max candidate:",max_can)
print("Min candidate:",min_can)
print("Max word:",max_word)
print("min word:",min_word)
print("ave word:",ave_word/len(all_text))



