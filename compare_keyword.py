def compare(candidate, list_of_keyword):
    f_max = 0
    list_sub = []
    for w in list_of_keyword:
        if candidate.lower() in w.lower():
            if f_max < (len(candidate.split(" ")) / len(w.split(" "))):
                f_max = len(candidate.split(" ")) / len(w.split(" "))

        if w.lower() in candidate.lower():
            list_sub.append(w.lower())
    for w in list_sub:
        temp = compare_ex(candidate,list_sub,w)
        if f_max<(temp/len(candidate.split(" "))):
            f_max = temp / len(candidate.split(" "))
    return f_max



def compare_ex(candidate,list_sub,match):
    temp = list_sub.copy()
    temp.remove(match)
    candidate2 = candidate.lower().strip(match)
    count = len(match.split(" "))
    for w in temp:
        if w in candidate2:
            candidate2 = candidate2.strip(w)
            count+=len(w.split(" "))
    return count
