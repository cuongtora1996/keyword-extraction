﻿{
  "link": "http://dantri.com.vn/suc-khoe/an-nom-sua-ngay-nang-nong-10-nguoi-nhap-vien-cap-cuu-20180705083012597.htm",
  "content": "Ăn nộm sứa ngày nắng nóng, 10 người nhập viện cấp cứu. 10 người trú tại xã Ích Hậu, huyện Lộc Hà (Hà Tĩnh) vừa bị ngộ độc thực phẩm phải nhập viện do ăn nộm sứa. Trạm trưởng Trạm Y tế xã Ích Hậu - Hồ Thế Ngọc cho biết, vào lúc 1h sáng ngày 3/7, trạm tiếp nhận bệnh nhân Nguyễn Thị Duyên (18 tuổi, ở thôn Ích Mỹ) với các triệu chứng nôn, đau bụng, đi ngoài nhiều lần. Không lâu sau lại có thêm 2 trường hợp có các triệu chứng trên nhập trạm. Cùng ngày 3/7, Bệnh viện Đa khoa huyện Lộc Hà cũng tiếp nhận 7 trường hợp nhập viện với các triệu chứng nôn, đau bụng, đi ngoài. Tất cả các bệnh nhân này đều ở thôn Ích Mỹ, xã Ích Hậu. Giám đốc Trung tâm YTDP huyện Lộc Hà Đào Duy Thế cho biết: Ngay sau khi nhận được thông tin, trung tâm đã lập đoàn kiểm tra tại thôn Ích Mỹ và các cơ sở y tế có bệnh nhân ngộ độc đang điều trị. Kết quả cho thấy, trước khi xảy ra ngộ độc, cả 10 trường hợp này đã mua nộm nuốt (sứa) tại chợ Eo, thôn Thống Nhất, xã Ích Hậu về ăn. Sau một ngày được chăm sóc y tế, tất cả các bệnh nhân trên đều đã ổn định và xuất viện. Hiện chưa thấy xuất hiện ca ngộ độc mới.",
  "keyword": [
    "sứa",
    "nộm sứa",
    "ngộ độc",
    "triệu chứng"
  ]
}