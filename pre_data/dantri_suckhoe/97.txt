﻿{
  "link": "http://dantri.com.vn/suc-khoe/phat-hien-hon-30-tan-thit-dong-lanh-khong-ro-nguon-goc-xuat-xu-20180719074304872.htm",
  "content": "Phát hiện hơn 30 tấn thịt đông lạnh không rõ nguồn gốc, xuất xứ. Chiều 18/7, Phòng Cảnh sát môi trường, Công an TP Cần Thơ vừa phát hiện và tạm giữ hơn 30 tấn thực phẩm đông lạnh không rõ nguồn gốc tại một cơ sở đông lạnh trên địa bàn. Ngành chức năng phát hiện 729 kg đầu gà đông lạnh hết hạn sử dụng tại cơ sở Hồng Thắm. rước đó, lực lượng chức năng phát hiện mô tô biển số 65B1-754.95 vận chuyển thực phẩm đông lạnh đi tiêu thụ nhưng không có hóa đơn, chứng từ chứng minh nguồn gốc, xuất xứ. Sau đó, lực lượng công an đã phối hợp với Chi cục Quản lý thị trường kiểm tra cơ sở Hồng Thắm ở 280A KV Yên Trung, phường Lê Bình, quận Cái Răng, TP Cần Thơ. Tại đây, lực lượng chức năng phát hiện số lượng lớn thịt động vật không rõ nguồn gốc. Cụ thể: 729 kg đầu gà đông lạnh hết hạn sử dụng, 612 kg thực phẩm (dê, chim, vú heo, chân gà, mực, bò viên…) không rõ nguồn gốc . Ngoài ra còn có hơn 29,5 tấn thực phẩm là trâu, bò heo, gà, cá… do nước ngoài sản xuất có tem nhãn đơn vị nhập khẩu. Tuy nhiên, chủ cơ sở chưa xuất trình được hóa đơn, chứng từ chứng minh nguồn gốc, xuất xứ của toàn bộ thực phẩm trên. Lực lượng chức năng đã lập biên bản và tạm giữ toàn bộ số thực phẩm trên nhưng giao cho cơ sở Hồng Thắm tự quản lý, bảo quản, chịu trách nhiệm.",
  "keyword": [
    "thịt đông lạnh",
    "thực phẩm đông lạnh",
    "không rõ nguồn gốc",
    "xuất xứ"
  ]
}