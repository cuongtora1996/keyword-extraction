from pyvi import ViPosTagger,ViTokenizer
from underthesea import pos_tag,ner,word_tokenize
import json,random
from collections import Counter
import sys
import pickle
from pathlib import Path
import re
import math
import pprint
import numpy as np
import nltk
import fnmatch
import os
from nltk.chunk import RegexpParser
from nltk import ngrams
from prettytable import PrettyTable
with open("stopwords.txt", "r", encoding="utf8") as f:
    all_stop_words = f.read()
    all_stop_words = all_stop_words.split("\n")

def extract_PoS_pattern(document):
    a = ViPosTagger.postagging(document)
    tagged = []
    for i, word in enumerate(a[0]):
        tagged.append((word, a[1][i]))


    chunkGram = r""" PHRASE:
                        {(<N.*>+ <A>* <E>)? <N.*>+ <A>*}
                """
    chunkParser = RegexpParser(chunkGram)
    chunked = chunkParser.parse(tagged)
    candidate_keywords = []
    for tree in chunked.subtrees():
        if tree.label() == 'PHRASE':
            candidate_keyword = ' '.join([x for x,y in tree.leaves()])
            candidate_keywords.append(candidate_keyword)
            flag_start = True
            name = ""
            for n in ner(candidate_keyword):
                if flag_start == False:
                    if n[3] != "O":
                        flag_start = True
                        name = n[0]
                else:
                    if n[3] != "O":
                        name += " " + n[0]
                    else:
                        flag_start = False
                        name = re.sub(r"(\w)(\.)(\s)(?=(?:(\w)(?:[\.\s]|$)))", r'\1\2', name)
                        candidate_keywords.append(
                            name.replace(" / ", "/").replace(" - ", "-").replace(" & ", "&").strip())
                        name = ""

            if (flag_start == True):
                name = re.sub(r"(\w)(\.)(\s)(?=(?:(\w)(?:[\.\s]|$)))", r'\1\2', name)
                candidate_keywords.append(name.replace(" / ", "/").replace(" - ", "-").replace(" & ", "&").strip())

    count_candidate_keywords = dict(Counter(candidate_keywords).items())

    candidate_keywords = [key for key in candidate_keywords if len(key)>1 and key.lower() not in all_stop_words]

    return candidate_keywords

def extract_n_gram(document,n):
    candidate_keywords = ngrams(document.split(" "), n)
    candidate_keywords = [" ".join([w for w in gram]) for gram in candidate_keywords]
    candidate_keywords = [key for key in candidate_keywords if len(key) > 1 and key.lower() not in all_stop_words]
    return candidate_keywords

def extract_filter_nPoS(document,n):
    candidate_keywords = []
    candidate_keywords = ngrams(document.split(" "), n)
    candidate_keywords = " . ".join([" ".join([w for w in gram]) for gram in candidate_keywords])
    a = ViPosTagger.postagging(candidate_keywords)
    tagged = []
    for i, word in enumerate(a[0]):
        tagged.append((word, a[1][i]))

    chunkGram = r""" PHRASE:
                            {(<N.*>+ <A>* <E>)? <N.*>+ <A>*}
                    """
    chunkParser = RegexpParser(chunkGram)
    chunked = chunkParser.parse(tagged)
    candidate_keywords = []
    for tree in chunked.subtrees():
        if tree.label() == 'PHRASE':
            candidate_keyword = ' '.join([x for x, y in tree.leaves()])
            candidate_keywords.append(candidate_keyword)
            flag_start = True
            name = ""
            for n in ner(candidate_keyword):
                if flag_start == False:
                    if n[3] != "O":
                        flag_start = True
                        name = n[0]
                else:
                    if n[3] != "O":
                        name += " " + n[0]
                    else:
                        flag_start = False
                        name = re.sub(r"(\w)(\.)(\s)(?=(?:(\w)(?:[\.\s]|$)))", r'\1\2', name)
                        candidate_keywords.append(
                            name.replace(" / ", "/").replace(" - ", "-").replace(" & ", "&").strip())
                        name = ""

            if (flag_start == True):
                name = re.sub(r"(\w)(\.)(\s)(?=(?:(\w)(?:[\.\s]|$)))", r'\1\2', name)
                candidate_keywords.append(name.replace(" / ", "/").replace(" - ", "-").replace(" & ", "&").strip())
    count_candidate_keywords = dict(Counter(candidate_keywords).items())

    candidate_keywords = [key for key in candidate_keywords if len(key) > 1 and key.lower() not in all_stop_words]

    return candidate_keywords

def extract_filter_document_pOs(document):
    document = re.sub(r"(\w)(\.)(\s)", r"\1(0.0)", document)

    sent_text = nltk.sent_tokenize(document)
    print(len(sent_text))
    document_slit = round(len(sent_text) / 4)
    new_document = sent_text[:document_slit] + sent_text[-document_slit:]

    new_document = " ".join(new_document).replace("(0.0)", ". ")
    a = ViPosTagger.postagging(new_document)
    tagged = []
    for i, word in enumerate(a[0]):
        tagged.append((word, a[1][i]))

    chunkGram = r""" PHRASE:
                            {(<N.*>+ <A>* <E>)? <N.*>+ <A>*}
                    """
    chunkParser = RegexpParser(chunkGram)
    chunked = chunkParser.parse(tagged)
    candidate_keywords = []
    for tree in chunked.subtrees():
        if tree.label() == 'PHRASE':
            candidate_keyword = ' '.join([x for x, y in tree.leaves()])
            candidate_keywords.append(candidate_keyword)
            flag_start = True
            name = ""
            for n in ner(candidate_keyword):
                if flag_start == False:
                    if n[3] != "O":
                        flag_start = True
                        name = n[0]
                else:
                    if n[3] != "O":
                        name += " " + n[0]
                    else:
                        flag_start = False
                        name = re.sub(r"(\w)(\.)(\s)(?=(?:(\w)(?:[\.\s]|$)))", r'\1\2', name)
                        candidate_keywords.append(
                            name.replace(" / ", "/").replace(" - ", "-").replace(" & ", "&").strip())
                        name = ""

            if (flag_start == True):
                name = re.sub(r"(\w)(\.)(\s)(?=(?:(\w)(?:[\.\s]|$)))", r'\1\2', name)
                candidate_keywords.append(name.replace(" / ", "/").replace(" - ", "-").replace(" & ", "&").strip())

    count_candidate_keywords = dict(Counter(candidate_keywords).items())

    candidate_keywords = [key for key in candidate_keywords if len(key) > 1 and key.lower() not in all_stop_words]

    return candidate_keywords
length = 0
count = 0
all_text = []
for dir_path,dir_names,file_names in os.walk("data"):
    for dir in dir_names:
        length += len(fnmatch.filter(os.listdir(os.path.join(dir_path, dir)), "*.txt"))

    for file in file_names:
        count += 1
        with open(os.path.join(dir_path, file), 'r+', encoding="utf8") as f:
            text = f.read()
            all_text.append(json.loads(text))

            sys.stdout.write("\r Process: " + str(round(count * 100 / length, 2)) + "%")
            sys.stdout.flush()
#End ===
all_pos_candidates = []
all_gram3_candidates = []
all_gram5_candidates = []
all_gram10_candidates = []
all_filter3_candidates = []
all_filter5_candidates = []
all_filter10_candidates = []
all_filter_document_candidates=[]
result_pos_candidates = []
result_gram3_candidates = []
result_gram5_candidates = []
result_gram10_candidates = []
result_filter3_candidates = []
result_filter5_candidates = []
result_filter10_candidates = []
result_filter_document_candidates=[]
count = 0
all_keywords =[]
for text in all_text:

    pos_candidates = extract_PoS_pattern(text['content'])
    # gram3_candidates = extract_n_gram(text['content'],3)
    # gram5_candidates = extract_n_gram(text['content'], 5)
    # gram10_candidates = extract_n_gram(text['content'], 10)
    # filter3_candidates = extract_filter_nPoS(text['content'],3)
    # filter5_candidates = extract_filter_nPoS(text['content'], 5)
    # filter10_candidates = extract_filter_nPoS(text['content'], 10)
    filter_documents_candidates = extract_filter_document_pOs(text['content'])
    count += 1
    all_keywords.extend(set(text['keyword']))
    #
    for candidate in set(pos_candidates):
        if candidate.lower() in [w.lower() for w in text['keyword']]:
            result_pos_candidates.append(1)
        else:
            flag = True
            for w in candidate.lower().split(" "):
                if w not in [a.lower() for a in text['keyword']]:
                    flag = False
            if flag == True:
                result_pos_candidates.append(1)
            else:
                result_pos_candidates.append(0)
    # for candidate in set(gram3_candidates):
    #     if candidate.lower() in [w.lower() for w in text['keyword']]:
    #         result_gram3_candidates.append(1)
    #     else:
    #         flag = True
    #         for w in candidate.lower().split(" "):
    #             if w not in [a.lower() for a in text['keyword']]:
    #                 flag = False
    #         if flag == True:
    #             result_gram3_candidates.append(1)
    #         else:
    #             result_gram3_candidates.append(0)
    # for candidate in set(gram5_candidates):
    #     if candidate.lower() in [w.lower() for w in text['keyword']]:
    #         result_gram5_candidates.append(1)
    #     else:
    #         flag = True
    #         for w in candidate.lower().split(" "):
    #             if w not in [a.lower() for a in text['keyword']]:
    #                 flag = False
    #         if flag == True:
    #             result_gram5_candidates.append(1)
    #         else:
    #             result_gram5_candidates.append(0)
    # for candidate in set(gram10_candidates):
    #     if candidate.lower() in [w.lower() for w in text['keyword']]:
    #         result_gram10_candidates.append(1)
    #     else:
    #         flag = True
    #         for w in candidate.lower().split(" "):
    #             if w not in [a.lower() for a in text['keyword']]:
    #                 flag = False
    #         if flag == True:
    #             result_gram10_candidates.append(1)
    #         else:
    #             result_gram10_candidates.append(0)
    # for candidate in set(filter3_candidates):
    #
    #     if candidate.lower() in [w.lower() for w in text['keyword']]:
    #         result_filter3_candidates.append(1)
    #     else:
    #         flag = True
    #         for w in candidate.lower().split(" "):
    #             if w not in [a.lower() for a in text['keyword']]:
    #                 flag = False
    #         if flag == True:
    #             result_filter3_candidates.append(1)
    #         else:
    #             result_filter3_candidates.append(0)
    # for candidate in set(filter5_candidates):
    #     if candidate.lower() in [w.lower() for w in text['keyword']]:
    #         result_filter5_candidates.append(1)
    #     else:
    #         flag = True
    #         for w in candidate.lower().split(" "):
    #             if w not in [a.lower() for a in text['keyword']]:
    #                 flag = False
    #         if flag == True:
    #             result_filter5_candidates.append(1)
    #         else:
    #             result_filter5_candidates.append(0)
    # for candidate in set(filter10_candidates):
    #     if candidate.lower() in [w.lower() for w in text['keyword']]:
    #         result_filter10_candidates.append(1)
    #     else:
    #         flag = True
    #         for w in candidate.lower().split(" "):
    #             if w not in [a.lower() for a in text['keyword']]:
    #                 flag = False
    #         if flag == True:
    #             result_filter10_candidates.append(1)
    #         else:
    #             result_filter10_candidates.append(0)

    for candidate in set (filter_documents_candidates):
        if candidate.lower() in [w.lower() for w in text['keyword']]:
            result_filter_document_candidates.append(1)
        else:
            flag = True
            for w in candidate.lower().split(" "):
                if w not in [a.lower() for a in text['keyword']]:
                    flag = False
            if flag == True:
                result_filter_document_candidates.append(1)
            else:
                result_filter_document_candidates.append(0)
    all_pos_candidates.extend(set(pos_candidates))
    # all_gram3_candidates.extend(set(gram3_candidates))
    # all_gram5_candidates.extend(set(gram5_candidates))
    # all_gram10_candidates.extend(set(gram10_candidates))
    # all_filter3_candidates.extend(set(filter3_candidates))
    # all_filter5_candidates.extend(set(filter5_candidates))
    # all_filter10_candidates.extend(set(filter10_candidates))
    all_filter_document_candidates.extend(set(filter_documents_candidates))
    sys.stdout.write("\r Process: " + str(round(count * 100 / len(all_text), 2)) + "%")
    sys.stdout.flush()
#
ct_PoS = dict(Counter(result_pos_candidates).items())
# ct_gram3 = dict(Counter(result_gram3_candidates).items())
# ct_gram5 = dict(Counter(result_gram5_candidates).items())
# ct_gram10 = dict(Counter(result_gram10_candidates).items())
# ct_filter3 = dict(Counter(result_filter3_candidates).items())
# ct_filter5 = dict(Counter(result_filter5_candidates).items())
# ct_filter10 = dict(Counter(result_filter10_candidates).items())
ct_filter_document = dict(Counter(result_filter_document_candidates).items())

t = PrettyTable(['Method','Candidate','Correct'])
t.add_row(["All keyword in document",len(all_keywords),len(all_keywords)])
# t.add_row(["All 3-grams",len(all_gram3_candidates),ct_gram3[1 if 1 in ct_gram3 else 0]])
# t.add_row(["All 5-grams",len(all_gram5_candidates),ct_gram5[1] if 1 in ct_gram5 else 0])
# t.add_row(["All 10-grams",len(all_gram10_candidates),ct_gram10[1] if 1 in ct_gram10 else 0])
# t.add_row(["Filter 3-grams",len(all_filter3_candidates),ct_filter3[1]if 1 in ct_filter3 else 0])
# t.add_row(["Filter 5-grams",len(all_filter5_candidates),ct_filter5[1]if 1 in ct_filter5 else 0])
# t.add_row(["Filter 10-grams",len(all_filter10_candidates),ct_filter10[1]if 1 in ct_filter10 else 0])
t.add_row(["PoS Pattern",len(all_pos_candidates),ct_PoS[1]if 1 in ct_PoS else 0])
t.add_row(["Filter Document",len(all_filter_document_candidates),ct_filter_document[1] if 1 in ct_filter_document else 0])

print(t)


