from underthesea.pos_tag.model_crf import CRFPOSTagPredictor


def postagging(sentence, format=None):

    sentence = [w for w in sentence.split()]
    crf_model = CRFPOSTagPredictor.Instance()
    result = crf_model.predict(sentence, format)
    return result
