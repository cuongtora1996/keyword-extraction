import pprint as sout
import numpy as np
import json
from string import punctuation
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException
import time


options = Options()
options.add_argument("--incognito")
profile = webdriver.FirefoxProfile()

profile.set_preference("permissions.default.image", 2)
profile.set_preference("media.volume_scale", "0.0")
browser = webdriver.Firefox(firefox_options=options, firefox_profile=profile,
                            executable_path="C:/Users/cuong/venv/workspace/geckodriver")
browser.get("https://thanhnien.vn/suc-khoe/trang-2.html")

article_href = browser.find_elements_by_xpath("//div[@class='l-grid']//div[@class='relative']/article/a")
href = [h.get_attribute("href")for h in article_href]
count = 3
while(len(href)<77):
    browser.get("https://thanhnien.vn/suc-khoe/trang-"+str(count)+".html")
    article_href = browser.find_elements_by_xpath("//div[@class='l-grid']//div[@class='relative']/article/a")
    href.extend([h.get_attribute("href") for h in article_href])
    count+=1


for index, link in enumerate(href[1:],134):
    if index >165:
        try:

            f = open("./data-new/suckhoe_thanhnien_"+str(index)+".txt","w+",encoding='utf8')

            browser.get(link)
            title = browser.find_element_by_xpath("//h1[@class='details__headline']").text

            if title.strip()[-1] not in punctuation:
                title += "."
            sapo = browser.find_element_by_xpath("//div[@id='chapeau']").text
            if sapo.strip()[-1] not in punctuation:
                sapo += "."
            content = title.strip()+" "+sapo.strip()
            contents = browser.find_elements_by_xpath("//div[@id='abody']/div[not(@class) and not(article) and not(em)]")
            for t in contents:
                text = t.text.strip()
                if text[-1] not in punctuation:
                    text+="."
                content +=" "+text

            print(content)
            key_count = 0
            key_words = []
            while(key_count<10):
                key_count+=1
                keyword = input("Keyword "+str(key_count)+" :")
                key_words.append(keyword)
            data = {"link":link,"content":content,"keyword":key_words}
            f.write(json.dumps(data,ensure_ascii=False))
            f.close()

        except Exception as e:
            print(e)

browser.quit()