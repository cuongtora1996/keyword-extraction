from pyvi import ViPosTagger,ViTokenizer
from underthesea import pos_tag,ner,word_tokenize
import os

os.environ['KERAS_BACKEND'] = 'tensorflow'
import json,random
from collections import Counter
import sys
import pickle
from pathlib import Path
import re
import math
import pprint
import numpy as np
import nltk
from nltk.chunk import RegexpParser
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM,Bidirectional,GRU
from keras.layers import Conv1D, MaxPooling1D,Flatten
from keras.models import load_model
from keras import metrics
from keras import backend as K
from keras.callbacks import CSVLogger
from imblearn.over_sampling import SMOTE
import random
from string import punctuation

def get_features(idf,document,candidate,candidate_list,vector):

    features = vector.tolist()[0]
    #Frequency-based
    pl = len([w for w in candidate_list if w.lower() is candidate.lower()])
    plc = len([w for w in candidate_list if candidate.lower() in w.lower() and candidate.lower() is not w.lower()])

    f_frequency = ((1/2)*pl*pl+plc)**(1.0/2)
    f_thematic = f_frequency*idf
    features.append(f_thematic)
    #statistical
    features.append((math.log(1+len(candidate.split(" ")),10)*math.log(1+len(candidate),10))**(1.0/2))
    #grammaticial
    length = 0
    result = 0
    for n in ner(candidate):
        if n[3] != "O":
            result+=1
        length +=1
    features.append(result/length)
    #position
    position = 1/(document.lower().find(candidate.lower())**(1.0/2)+1)

    features.append(position)

    return features

