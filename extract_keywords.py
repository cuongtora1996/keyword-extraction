from pyvi import ViPosTagger,ViTokenizer
from keras_metric_custom import fmeasure,recall,precision
from underthesea import pos_tag,ner,word_tokenize
import os
from gensim.models import Word2Vec
from collections import Counter
from pathlib import Path
import re,math,pprint,pickle,sys,json,random
import numpy as np
from nltk.chunk import RegexpParser
from keras.models import Sequential,load_model

from imblearn.over_sampling import SMOTE
import random
from string import punctuation
from keras_metric_custom import fmeasure,recall,precision
import fnmatch

with open("stopwords.txt", "r", encoding="utf8") as f:
    all_stop_words = f.read()
    all_stop_words = all_stop_words.split("\n")


def token(document):
    text_all = document.strip().replace(".\n", " . ").replace("\n", " . ").replace(" - ", " 0-0 ").replace(
        " / ", " 0/0 ").replace(" & ", " 0&0 ")


    textcontent = word_tokenize(text_all, format="text")
    splash = " / "
    middle = " - "
    andsym = " & "

    textcontent = textcontent.replace(splash, "/").replace(middle, "-").replace(andsym, "&").replace("0-0",
                                                                                                     "-").replace("0/0",
                                                                                                                  "/").replace(
        "0&0", "&").replace(" ’ ", "’")
    textcontent = re.sub(r"([a-zA-Z])(\.)(\s)(?=(?:([a-zA-Z])(?:[\.\s])))", r'\1\2', textcontent)
    return textcontent

def extract_candidate_keyword(document):
    a = ViPosTagger.postagging(document)
    tagged = []
    for i, word in enumerate(a[0]):
        tagged.append((word, a[1][i]))

    chunkGram = r""" PHRASE:
                            {(<N.*>+ <A>* <E>)? <N.*>+ <A>*}
                    """
    chunkParser = RegexpParser(chunkGram)
    chunked = chunkParser.parse(tagged)
    candidate_keywords = []
    for tree in chunked.subtrees():
        if tree.label() == 'PHRASE':
            candidate_keyword = ' '.join([x for x, y in tree.leaves()])
            candidate_keywords.append(candidate_keyword.strip())
    candidate_keywords = [key for key in candidate_keywords if
                          len(key) > 1 and len(key.split(' ')) < 5 and key.lower() not in all_stop_words]

    return candidate_keywords

def get_features(idf,document,candidate,candidate_list,vector):
    features = vector.tolist()[0]
    #Frequency-based
    pl = len([w for w in candidate_list if w.lower() is candidate.lower()])
    plc = len([w for w in candidate_list if candidate.lower() in w.lower() and candidate.lower() is not w.lower()])
    f_frequency = ((1/2)*pl*pl+plc)**(1.0/2)
    f_thematic = f_frequency*idf
    features.append(f_thematic)
    #statistical
    features.append((math.log(1+len(candidate.split(" ")),10)*math.log(1+len(candidate),10))**(1.0/2))
    #grammaticial
    length = 0
    result = 0
    for n in ner(candidate):
        if n[3] != "O":
            result+=1
        length +=1
    features.append(result/length)
    #position
    position = 1/(document.lower().find(candidate.lower())**(1.0/2)+1)

    features.append(position)

    return features


def predict(document):
    word2vec_model = Word2Vec.load("word2vec_model_final2")
    idfs = dict(np.load('idf.npy').item())
    document = token(document)
    candidates = extract_candidate_keyword(document)

    text_lower = " ".join([w.lower() for w in document])

    filter_candidates = []
    feature_list=[]
    for candidate in set(candidates):
        if candidate.lower() not in [w.lower() for w in filter_candidates]:
            filter_candidates.append(candidate)

    print(filter_candidates)

    for candidate in filter_candidates:
        if candidate in idfs:
            idf = math.log(300/idfs[candidate],10)
        else:
            idf = math.log(301,10)
        a = np.zeros(shape=(1, 300))
        for w in candidate.split():
            s = w
            if w[-1] in punctuation:
                s = w[0:-1]
            if s!='' and s in word2vec_model.wv.vocab.keys():
                a+= np.array(word2vec_model.wv[s])

        features = get_features(idf,document,candidate,candidates,a)


        feature_list.append(features)

    # load model from single file
    model = load_model('Bi_LSTM_model.h5',custom_objects={"precision":precision,"recall":recall,"fmeasure":fmeasure})
    features = np.zeros((len(feature_list), 304), dtype=float)
    for i, row in enumerate(feature_list):
        features[i, -len(row):] = np.array(row)[:304]

    features = np.reshape(features,(features.shape[0],features.shape[1],1))
    scores = model.predict(features)
    wc = {}
    for i in range(len(scores)):
        wc[filter_candidates[i]] = float(scores[i])

    return wc


# with open('data/Test/dantri/1.txt','r',encoding='utf8') as f:
#
#     data = json.loads(f.read())
#     predict(data['content'])

