from gensim.models import KeyedVectors,Word2Vec,fasttext
from difflib import SequenceMatcher,get_close_matches
from underthesea import ner
import sys,os,json,re
import fnmatch
from string import punctuation
import nltk
#Get all text
length = 0
count = 0
all_text = []
keyword_list = []
feature_list = []
result_list = []
n = 0

for dir_path,dir_names,file_names in os.walk("data2/Train"):
    for dir in dir_names:
        length += len(fnmatch.filter(os.listdir(os.path.join(dir_path, dir)), "*.txt"))

    for file in file_names:
        count += 1
        with open(os.path.join(dir_path, file), 'r+', encoding="utf-8") as f:
            text = f.read()
            all_text.append(json.loads(text))

            sys.stdout.write("\r Process: " + str(round(count * 100 / length, 2)) + "%")
            sys.stdout.flush()
dictionary = []
for text in all_text:
    for w in text['content'].split():

        if w[-1] in punctuation:
            w = w[0:-1]
        if w =='':
            continue
        if w not in punctuation:
            dictionary.append(w)
print(len(set(dictionary)))
# with open('report/dictionary2.txt','w+',encoding="utf8") as f:
#     f.write("\n".join(set(dictionary)))
#     f.close()
# #GET MATCH--------------------------------------------------
#
# word2vec = KeyedVectors.load("word2vecSCvn")
#
# dictionary_match = []
# dictionary_not_match = []
# for w in set(dictionary):
#     if w in word2vec.wv.vocab.keys():
#         dictionary_match.append(w)
#
#     else:
#         t = w.split("_")
#         check = 0
#         a = 0
#         flag = False
#         seq = []
#         for i in range(0, len(t)):
#             if check == 0:
#                 if t[i] not in word2vec.wv.vocab.keys():
#                     check = 1
#                 else:
#                     if i == len(t) - 1:
#                         flag = True
#                     seq.append(0)
#             elif check == 1:
#                 if str(t[i - 1] + "_" + t[i]) not in word2vec.wv.vocab.keys():
#                     break
#                 else:
#                     if i == len(t) - 1:
#                         flag = True
#                     check = 0
#                     seq.append(1)
#         if flag == True:
#             dictionary_match.append(w)
#         else:
#             dictionary_not_match.append(w)
# with open('report/word_match2.txt','w+',encoding="utf8") as f:
#     f.write("\n".join(set(dictionary_match)))
#     f.close()
#
# with open('report/word_not_match2.txt','w+',encoding="utf8") as f:
#     report = [0]*10
#     for w in set(dictionary_not_match):
#         n = ner(w)[0]
#         f.write(n[0]+':'+n[1]+','+n[3]+'\n')
#         c = w.count('_')
#         report[c+1] +=1
#
#     for r in range(1,10):
#         if(report[r]!=0):
#             f.write(str(r)+'từ:'+str(report[r])+',')
#
#     f.close()

# # Save match----------------------------------------------------------
# model_train = Word2Vec(size=300,min_count=1,negative=0,window=5,sg=0,iter=500,workers=4)
# model_train.build_vocab([dictionary_match])
# for w in model_train.wv.vocab.keys():
#     if w in word2vec.wv.vocab.keys():
#         model_train.wv.syn0[model_train.wv.vocab[w].index] = word2vec.wv[w]
#
#     else:
#         t = w.split("_")
#         check = 0
#         a = 0
#         flag = False
#         seq = []
#         for i in range(0,len(t)):
#             if check == 0:
#                 if t[i] not in word2vec.wv.vocab.keys():
#                     check = 1
#                 else:
#                     if i == len(t)-1:
#                         flag = True
#                     seq.append(0)
#             elif check == 1:
#                 if str(t[i-1]+"_"+t[i]) not in word2vec.wv.vocab.keys():
#                     break
#                 else:
#                     if i == len(t)-1:
#                         flag = True
#                     check = 0
#                     seq.append(1)
#         if flag == True:
#             a = [0] * 300
#             temp = w.split("_")
#             index = 0
#             for i in seq:
#                 if i == 0:
#                     a = [x + y for x, y in zip(a, word2vec.wv[t[index]])]
#                     index+=1
#                 else:
#                     a = [x + y for x, y in zip(a, word2vec.wv[t[index]+"_"+t[index+1]])]
#                     index+=2
#
#             model_train.wv.syn0[model_train.wv.vocab[w].index] = a
#         else:
#             print(w)
#
#
# model_train.wv.save_word2vec_format("word2vecSV_match2",binary=True)

##Final
all_sentence = []

for w in all_text:
    document = re.sub(r"(\w)(\.)(\s)", r"\1(0.0)", w['content'])

    sent_text = nltk.sent_tokenize(document)

    for text in sent_text:
        all_sentence.append(text.replace("(0.0)", ". "))
print(len(all_sentence))
print("true")
model = Word2Vec(size=300,min_count=1,negative=0,window=5,sg=0,iter=500,workers=4)
model.build_vocab([dictionary])
# training_examples_count = len(all_text)
training_examples_count = len(all_sentence)
# model.build_vocab([list(model_train.wv.vocab.keys())],update=True)
# model.intersect_word2vec_format("word2vecSV_match2",binary=True, lockf=0.0)
# model.train([w['content'].split(" ") for w in all_text],total_examples=training_examples_count, epochs=model.iter)
model.train([w.split(" ") for w in all_sentence],total_examples=training_examples_count, epochs=model.iter)
model.save('word2vec_model_final2_nb')
print("true")

# with open('report/word2vec2.txt','w+',encoding='utf8') as f:
#
#     for key in model.wv.vocab.keys():
#         if(key in model_train.wv.vocab.keys()):
#             if(list(model.wv[key]) == list(model_train.wv[key])):
#                 f.write(str(key)+":"+str(list(model.wv[key]))+'- MATCH\n')
#             else:
#                 f.write(str(key)+":"+ str(list(model.wv[key]))+'- NOT MATCH\n')
#         else:
#             f.write(str(key)+ ":"+ str(list(model.wv[key]))+ '- NOT MATCH\n')

## FASTTEXT------------------------------------
#
# word2vec = KeyedVectors.load("fasttext_model")
# print(len(word2vec.wv.vocab))
# model = Word2Vec(size=300,min_count=1,negative=0,window=5,sg=0,iter=500,workers=4)
#
# model.build_vocab([w['content'].split() for w in all_text])
#
# print(len(model.wv.vocab))
# training_examples_count = model.corpus_count
# model.build_vocab([list(word2vec.wv.vocab.keys())], update=True)
#
# print(len(model.wv.vocab))
# model.intersect_word2vec_format("cc.vi.300.vec.gz",binary=False, lockf=1.0)
#
# model.train([w['content'].split(" ") for w in all_text],total_examples=training_examples_count, epochs=model.iter)
#
# model.save("word2vec_model_retrain")
# model.wv.save("word2vec_model_retrain_vector")
#
#
#
# model = Word2Vec.load("word2vec_model_retrain")
# print(model.wv['bệnh_viện'])

